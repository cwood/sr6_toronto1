House Rules, GM Rulings, etc.
=============================

[[_TOC_]]

Astral Homecoming And Trains
----------------------------

If a mage goes wandering in the astral, the train will have moved. However it will be easily found, as the mage can easily find the train tracks (devoid of life) and follow them along to the train.

A mage can always find their body on a train moving along a predictable route.

Assault Cannons And Silencers
-----------------------------

Single barrelled assault cannons (read: squad/platoon/company level fully automatic machine guns) can equip silencers.

Availability For Unspecified Ammunition
---------------------------------------

The availability (SR6 pp 244) and legality (SR6 pp 245) of ammunition with no listed legality are as follows:

* The legality is illegal.
* The availability is as the most available weapon that this ammunition can be used with.

Example:

* See assault cannon ammunition (SR6 pp 262), as available as the most available assault cannon (SR6 pp 258) and illegal to possess.


Burned SINs
-----------

A SIN may be burned (made ineffective) by deleting it from all local devices and never redownloading or referring to it in any way. The SIN may still exist in other databases to be discovered at some future point by parties unknown who will then not be able to link the SIN with the person.

Control Rigs Help With All Rigging Tests
----------------------------------------

Control rigs (SR6 pp 197,283) apply their bonuses to every single test involving the non-Matrix, non-Magical operation of a vehicle or drone. (Matrix attacks and actions are not covered by control rig bonuses.)

Cyberware Scanners
------------------

Cyberware scanners are specifically looking for the things that distinguish cyberware from other types of hardware. Cyberware will only find cyberware, and not anything else, including but not limited to:

* ceramics
* creatures
* drones
* drugs
* electronics
* weapons
* etc.

Cyberware Secondary Effects Are As The Assensing Table
------------------------------------------------------

The secondary effects of augmentations are not observable through Assensing (SR6 pp pp 159) unless the appropriate test thresholds a specific augmentation's grade has been reached.

Secondary effects of augmentations include but are not limited to:

* rapid movements from Wired Reflexes (SR6 pp 287)
* altered thoughts patterns from control rigs (SR6 pp 197,283) and cyberjacks (SR6 pp 176,283)
* astral blankness from cyberlimbs (SR6 pp 288)

For example, two net hits will show the rapid movements and twitchiness from standard grade wired reflexes but miss an alphaware cyberlimb.

Detect Magic From The Astral Plane
----------------------------------

The spell Detect Magic (SR6 pp 135) will detect magic outside of the Astral Plane (SR6 pp 159, "spells glow brightly on the astral") within the usual constraints of the spell including the standard caveats for masking metamagic.

Disconnected Datachips
-----------------------

Datachips (SR6 pp 269) are not accessible over the Matrix by any Matrix or Technomancer abilities when powered off. Accessing data stored on a datachip requires that the datachip be plugged into an active Matrix device or host.

Drone Firewalls
---------------

The value for a drone's firewall defaults to the drone's Sensor attribute.

Drone Close (Melee) Combat
--------------------------

This section is focused on two melee combat cases:

* An unrigged drone, whether or not that drone is being controlled by a Matrix creature.
* A rigged drone.

In this section, "Piloting" is the skill (SR6 pp 97) and, "Pilot", "Sensor" are the vehicle/drone attributes (SR6 pp 199,201). (Control Rig bonuses, SR6 pp 197,283, apply to all these tests, see the section on rigging tests.) The table "Control Rigs" (SR6 pp 197) will indicate how Rigger attributes map when jumped in.

Using "Pilot" and "Sensor" here reflects the inherent limits of a drone's mechanisms and software in the free-flowing medium of melee combat.

Armed melee combat attacks:

* Use the weapon's attack rating.
* Drone dice pool is [Weapon] Targeting autosoft+Pilot.
* Jumped-in rigger dice pool is Close Combat+Logic.

Unarmed melee combat attacks:

* Drone
  * Attack rating is Pilot+Sensor.
  * Dice pool is Unarmed Combat autosoft+Pilot.
* Jump-in Rigger
  * Attack rating is Charisma(Strength)+Intuition(Reaction).
  * Jumped-in rigger dice pool is Close Combat+Logic(Agility).

Melee combat defence:

* Defence rating is drone Body+Armour.
* Drone dice pool is Pilot+Evasion autosoft.
* Jumped-in rigger dice pool is Intuition(Reaction)+Intuition.

Drone Matrix Note
-----------------

For the purposes of any Matrix interactions drones are considered to be in Hot-Sim (SR6 pp 174, "Virtual Reality").

Extended Test Threshold Guidelines (Provisional)
------------------------------------------------

These are the Extended Test analogies of the Threshold Guidelines (SR6 pp 36) for tests.

* 2 - simple task
* 8 - more complex
* 15 - basic Shadowrunner difficulty
* 26 - more difficult, impressive
* 40 - tricky
* 57 - elite-level
* 77 - standing out among the elite

Fake SINs And Fake Licenses
---------------------------

References:

* legality (SR6 pp 245)
* fake SINs (SR6 pp 272)
* fake licenses (SR6 pp 273)

My reading is that the rating of the fake SIN/license is the resistance rating for both verifying your own ID (example: identification check on you as a person) and verifying the legitimacy of the fake SIN/license itself (example: checking the SIN/license to see if it's fake).

There is a difference between somebody checking that you are presenting a valid SIN/license for the purchase at hand (alcohol, firearms) in a cursory way and actually putting effort into the matter. Only the latter requires a test.

Perception is linked with Intuition, and most people aren't trained in Perception, so their dice pool will be Intuition -1 anyway. Add in the number of people who aren't really diligent about checking SINs/licenses and there are definitely uses for low-threshold SINs/licenses.

Invisibility Status Number
--------------------------

The "#" number of invisibility status is the number of net hits on the Magic+Sorcery roll.

Invisibility#0 means that if somebody is deliberately looking they will automatically find you, but not see you if they aren't specifically looking. (This status will never happen unless there's a tie-goes-to-winner opposed roll.)

Jack Jockey Internal Defense Sources
------------------------------------

Any logged-in active defenders, including but not limited to security spiders and intrusion countermeasures (IC/ICE), are considered internal defence sources not covered by Jack Jockey (H&S pp 82).

Lift/Carry
----------

This can be performed by the player's choice of strength or body.

Live Feeds
----------

Devices with microphones, cameras, and other sensors (commlinks, security cameras, traffic cameras, seismic sensors, ultrasound sensors) keep a local rolling buffer of feed data equal to twice their Data Processing in seconds.

Hosts that receive live feeds often have default settings to store this data for two to the power of their Data Processing in days. Unless otherwise noted as a plot point this is how many days you can go back when searching live feed data.

Copying live feed data out of a host requires one Edit File (SR6 pp 181) action per twenty combat rounds (3 seconds per round per SR 6 pp 40, so one minute of live feed copied per Edit File action) of a single live feed data source. This may be delegated to one or more other Matrix users, including sprites, who will roll their own Edit File tests.

Analyzing a single live feed's data to locate the specific span of three seconds in which the desired data points are located will use the Hash Check procedure (SR6 pp 182) with the threshold of 1 being if the searcher knows the approximate timestamp (to the minute) and 4 if the searcher does not know this. If the searcher already knows the timestamp to the second then a single success on the Hash Check procedure will locate the specific time span.

If the searcher has found a specific span of three seconds in one live feed storage then they will have this span for any other live feed stored on this host. For example, if a searcher has found video then they will also know where the thermal sensor data for the same time span is stored.

Magazines Versus Clips
----------------------

These use the same Reload Weapon (SR6 pp 44) major action or Reload Smartgun (SR6 pp 42) minor action no matter whether the weapon has an external magazine/clip or an internal magazine.

Matrix Action Legality
----------------------

Illegal Matrix actions that avoid some future Overwatch Score do themselves incur Overwatch Score through their own illegality. Examples include but are not limited to:

* Backdoor Entry (SR6 pp 180)
* Known Exploit Backdoor Entry (H&S pp 30)

Matrix Strength Indicators
--------------------------

These came about after a discussion concerning, in parts, how it is difficult to tell just how strong a Matrix host or Intrusion Countermeasures or Sprite is when there are no adjectives used (example: "a Patrol IC is here").

Matrix hosts can be described with the graphical overlays that host owners use to decorate their hosts. For instance, a Stuffer Shack host might look like a Stuffer Shack with security guards (IC) dressed in Aztechnology uniforms. The strength of these hosts and IC can be judged by their simulated gear.

* Host Rating 1-3 (near trivial: local chain or non-profit or village government)
  * light weapons (batons, knives, light pistols)
  * no body armour
  * militia or security guards
  * small stores, waiting rooms, lobbies
* Host Rating 4-6 (a fight: large corporation or government or institution)
  * medium weapons (swords, axes, heavy pistols, SMGs)
  * light body armour
  * security troopers
  * chain stores, showrooms, shopping malls
* Host Rating 7-9 (a challenge: megacorp or major institution or government)
  * assault weapons (polearms, great swords/axes, automatic rifles)
  * heavy body armour
  * corporate/governmental soldiery
  * police stations, forts, laboratories
* Host Rating 10+ (deadly: megacorp/government/organization high security zone)
  * heavy weapons (siege gear, assault cannons, missile launchers)
  * specialist body armour
  * corporate/governmental special forces
  * fortresses, dungeons, military bases, emplacements

Minimum Drain And Minimum Fade
------------------------------

The minimums of both Drain (SR6 pp 127) and Fade (SR6 pp 189 under Complex Forms) are 1. Abilities that reduce Drain/Fade do not reduce those lower than 1.

Personal Area Networks, book rules
----------------------------------

Non-device personas cannot be protected behind a PAN (SR6 pp 173, lists only devices as being on a PAN). This includes anybody in AR/VR, sprites, AIs, Resonance creatures, et cetera.

The rigger has to deal with noise from the RCC on their person to the rigged vehicle/drone (SR6pp 176,197) for vehicles/drones not on a PAN.

Personal Area Networks, house rules
-----------------------------------

An RCC can be the the centre of a rigger's PAN, in the same way as a commlink or deck can be at the centre of a PAN.

For vehicles/drones on a decker's/technomancer's PAN, the rigger has to deal with noise from their RCC to the device hosting the PAN, and also from the device hosting the PAN to the vehicles/drones. Example:

> If a rigger is standing physically beside a drone, which is attached to a technomancer's PAN, but the technomancer is 11 km away (+3 noise) concealed deep in a dense thicket (+1 noise), the rigger's signal has to go to the technomancer through all that noise and then back. This means for a jumped-in rigger:
>
> * -4 dice pool for the noise in getting rigger's signal from rigger to technomancer.
> * -4 dice pool for the noise in getting rigger's signal from technomancer to drone
> * This scenario's total is -8 dice pool to all rolls when jumped in.

A vehicle/drone protected behind a PAN will look like the persona running the PAN. A Matrix Perception Test (SR6 pp 178, 182) with any hits will reveal that this is only a symbolic link to the main persona. However anybody will be able to attack the main persona just like it was right up close in the Matrix.

A persona within a host can still protect devices behind a PAN if those devices are outside the host. The persona cannot attack anything outside the host, but can defend against attacks. There are no dice pool penalties on either side for attacking the persona protecting the PAN or defending against those attacks.

Physical Actions While Jumped In
--------------------------------

Unless specifically stated in the rules, A Rigger performing actions using physical attributes or actions using skills whose primary linked attribute is physical gain no bonuses from those physical attributes while a Rigger is jumped in to a vehicle or drone. For example:

* The Rigger may use a major action to Stealth (SR6 pp 97) past a listening sentry, but will not use either their Stealth (linked with Agility) or Agility in the roll.
* The Rigger may use a minor action to Avoid Incoming (SR6 pp 41), but will not use their Athletics skill (linked with Agility) in the roll.
* The Rigger may use a minor action to Block (SR6 pp 41), but will not add their Close Combat skill (linked with Agility).
* The Rigger may use a minor action to Dodge (SR6 pp 41), but will not add their Athletics skill (linked with Agility).

Rigger Command Console Is Portable
----------------------------------

Like the decker's cyberdeck, the RCC can be strapped around a forearm like a gauntlet. While rather bulkier than the cyberdeck it works the same way as it would when not worn.

* 1 Major Action to detach the RCC from a vehicle.
* 1 Major Action to strap the RCC to a forearm.

Riggers' Matrix Presence
------------------------

Considering the rules in Rigging Through The Matrix (SR6 pp 197), as soon as a rigger jumps in (SR6 pp 44,182) to a rigged vehicle/drone, that rigged vehicle/drone's Matrix icon (SR6 pp 173) is replaced by the rigger's Matrix persona (SR6 pp 173,174). The rigged vehicle/drone ends up as an icon attached to the rigger's persona. This happens the instant the Jump In To Rigged Vehicle/Drone is performed.

This does mean that the rigger is on the Matrix and vulnerable to Matrix-based attacks against their persona if the rigger has wireless enabled, for instance by jumping into a drone that is not connected by cables to their RCC. The rigger may consider asking a nearby Decker or Technomancer for Matrix cover.

A rigger can be hidden or phantomed at any time after they enter VR. If they jump in to a device while hidden/phantomed then the device will be an icon on the rigger's persona, and thus by inference part of the hiding/phantoming. (Note that people will notice the metal drone flying around regardless of what's happening to icons and personas on the Matrix.) If a rigger is hidden/phantomed while jumped in then the same applies for the drone.

The rigger's VR Initiative replaces their regular initiative until the end of the turn where they jump out and switch interface modes (SR6 pp 184) to AR. Their number of actions remaining after jumping out and switching modes is taken from their regular action allocation as if they had already been spending those, not their AR/VR action allocation combined.

The rigger's AR initiative is used until the end of the turn where they switch interface modes to VR and jump in. Their number of actions remaining after switching interface modes and jumping in is taken from their VR action allocation as those they had already been spending those, not their AR/VR action allocation combined.

(For example, if a rigger has 1M/2m in AR, but starts the turn in VR with 3M/4m, and uses only the minor action Switch Interface Mode to move to AR before jumping out, that rigger will only have 1M/1m actions remaining.)

Rules That Deserve A Close Reading
----------------------------------

* Social Rating From Armor (FS pp 50)

Sixth World Companion Rules Used
--------------------------------

These are the Sixth World Companion rules that we are currently using:

* Expanded Roles For Strength (SWC pp 150)
  * Close Combat: Strength For Damage, Agility For Attack Rating
  * High Strength Adds To Damage
  * High Strength Reduces Recoil
* Magic Can't Affect Free Will (SWC pp 147), including but not limited to:
  * Control Actions (SR6 pp 140)
  * Control Thoughts (SR6 pp 140)
  * Influence critter power (SR6 pp 225)
  * Mind Link (SR6 pp 135)
  * Mind Probe (SR6 pp 135)
* Soirees And Socialites (SWC pp 156-157)
  * Opposed Social Skills
  * Point And Counterpoint
  * The Social Condition (Monitor)
    * The rounds here will be at plot-driven inflection points not every single minute by the in-game clock.
  * Social Maneuvers
    * The New Social Moves from Smooth Operations pp 46 augment this section.
* Up To Six Minor Actions (SWC pp 151)
* Working For The Man/People/Organleggers (SWC pp 153)
* Wild Die Extra Actions (SWC pp 159)

Spirits Materialized In Our Plane
---------------------------------

These not not dual-natured, they are just like any other creature/person on the plane while materialized. Dual Natured (SR6 pp 223) is a separate power and separate effect from Materialization (SR6 pp 225).

Spirits Summoned In The Astral Plane
------------------------------------

These are not dual-natured, they are Astral-only just like if they had the Astral Form critter power (SR6 pp 222) unless they have Materialization (SR6 pp 225).

Sprites And Regular Autopiloted Drones/Vehicles
-----------------------------------------------

A sprite can control autopiloted, unrigged vehicles.

Machine sprites can instantly Override their Technomancer's own vehicle or drone (but nobody else's) with no roll, but it takes one sprite task to do the simple action of driving somewhere, considered "sustained".

If the sprite needs to undertake more complicated actions while driving (changing destinations, undertaking maneuvers, rolling tests, etc.) then each new action must be specifically directed by the Technomancer, and will consume a task.

Sprites And Rigged Drones/Vehicles
----------------------------------

Sprites cannot use "Jump In To Rigged Device" (SR6 pp 182) because sprites do not have one of the important prerequsites, namely either an implanted control rig (SR6 pp 283) or a Technomancer Machine Mind Echo (SR6 pp 195).

That said a Machine Sprite (SR6 pp 193) could control a device with or without a rigger interface on autopilot, given how a Machine Sprite has the Override ability (SR6 pp 194).

Teamwork Extended Tests
-----------------------

The helpers contribute their extra dice (SR6 pp 36) before the first roll of the leader's extended test roll (SR6 pp 36). All helpers and the leader are working through the whole time of the extended test. Glitches and critical glitches (SR6 pp 44) from any helper as well as the leader affect the whole teamwork extended test. 

Technomancers And Wired-Only Things
-----------------------------------

A data tap (SR6 pp 269) can be used to connect to a wired-only host through the data tap's wireless capability. The tap must be placed on the network cable or network jack and then host access is as if somebody was entering the host from a direct Matrix connection.

Tool (Kit/Shop/Facility) Uses
-----------------------------

The rules specify "building and repairing" (SR6 pp 273) so if your skill does not involve either of those there is no set of tools for it.

Torture
-------

Torture as used for the extraction of truthful information [does not work out of character](https://www.newscientist.com/article/mg22830471-200-torture-doesnt-work-says-science-why-are-we-still-doing-it/) and will not work in character for the same reasons.

Torture for any purposes will result in docked karma and very adversely affected reputation and heat scores (SR6 pp 232-237).

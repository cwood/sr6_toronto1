# Rules Hints And References

[[_TOC_]]

## Advancement Costs (SR6 pp 68)

|Type|Karma Cost|Training Time|
|---|---|---|
|Active skills|5 X new rank|(new rank) months|
|Specializations|5|1 month|
|Expertise|5|2 months|
|Attributes|5 X new rank|(new rank X 2) months|
|Knowledge Skills|3|1 month|
|Purchase positive quality|2 X normal cost|1 week|
|Eliminate negative quality|2 X normal cost|1 week|
|New spell|5 per spell|1 week|
|New complex form|5 per form|1 week|
|Initiation|10 + Initiate Grade|(Grade + 1) months|
|Submersion|10 + Submersion Level|(Level + 1)weeks|

Note that new spells and complex forms may not be purchased during character creation.

## Attribute-Only Tests (SR6 pp 67)

* Composure: Willpower+Charisma
* Judge Intentions: Willpower+Intuition
* Lift/Carry: Body+Willpower
  * Or Strength+Willpower at character's discretion
* Memory: Logic+Intuition

## Combat (SR6 pp 104)

These notes do not list items like changing weapon fire modes or using Edge though they are definitely implied below.

### Actions

* start with 1 Major and 1 Minor
* +1 Minor Action for each Initiative Die
* optional: use a Major Action to perform a Minor Action
* optional: use 4 Minor Actions to perform a Major Action

### Close Combat (SR6 pp 94)

#### Armed Close Combat (SR6 pp 94)

1. say your melee Attack Rating (Weapon Attack Rating+Strength)
1. compare to Defence Rating (Body+Armour+Armour Effects)
1. check other environmental advantages
1. distribute Edge
1. roll Close Combat+Agility vs Reaction+Intuition
1. damage is listed Weapon Damage+Net Hits soaked by Body (pool of dice)

#### Grapple (SR6 pp 111)

1. say your unarmed Attack Rating (Strength+Reaction)
1. compare to Defence Rating (Body+Armour+Armour Effects)
1. check other environmental advantages
1. distribute Edge
1. roll Close Combat+Agility (unopposed)
1. Strength+Net Hits vs Strength (pool of dice)
1. success means opponent is restrained

#### Unarmed Close Combat (SR6 pp 39, 94)

1. say your unarmed Attack Rating (Strength+Reaction)
1. compare to Defence Rating (Body+Armour+Armour Effects)
1. check other environmental advantages
1. distribute Edge
1. roll Close Combat+Agility vs Reaction+Intuition
1. damage is 2S+Net Hits soaked by Body (pool of dice)

### Ranged Combat (SR6 pp 104)

1. say your weapon's Attack Rating at that range
1. compare to Defence Rating (Body+Armour+Armour Effects)
1. check other environmental advantages
1. distribute Edge
1. roll Firearms+Agility vs Reaction+Intuition
1. Damage is listed Weapon Damage+Net Hits soaked by Body (pool of dice)

### Ranged Combat From A Vehicle (SR6 pp 104, SR6 pp 200)

1. say your weapon's Attack Rating at that range
1. compare to Defence Rating (Body+Armour+Armour Effects)
1. check other environmental advantages
1. distribute Edge
1. roll Firearms+Agility-(Speed Interval) vs Reaction+Intuition
1. Damage is listed Weapon Damage+Net Hits soaked by Body (pool of dice)

Note: The only difference from the Ranged Combat section is using the Speed Interval as a dice pool modifier.

### Vehicle Combat (SR6 pp 200, DC pp 143)

1. select a hardpoint-mounted weapon to attack with, note Attack Rating and Damage Value
1. add +1 Attack Rating each additional linked hardpoint-mounted weapon
1. add +1 Damage Value for each additional linked hardpoint-mounted weapon of DV 1-4
1. add +2 Damage Value for each additional linked hardpoint-mounted weapon of DV 5+
1. say your weapon's Attack Rating at that range
1. compare to Defence Rating (Body+Armour+Armour Effects)
1. check other environmental advantages
1. distribute Edge
1. roll (Engineering+Logic)-(Speed Interval) vs Reaction+Intuition
1. Damage is Damage Value+Net Hits soaked by Body (pool of dice)

Note: Attacking with linked weapons costs a Minor Action in addition to the Major Action expended to attack. You do not have to link weapons to attack.

### Thrown Grenades (SR6 pp 114,115)

#### Thrown Grenade Instructions

1. say your unarmed Attack Rating (Strength+Reaction), -1 (Close) or -6 (Near)
1. compare to Defence Rating (Body+Armour+Armour Effects)
1. check other environmental advantages
1. distribute Edge
1. roll Athletics+Agility (unopposed)
1. roll scatter, see next sections

#### Scatter At Range Table (SR6 pp 115)

|Range|Distance in Meters (Thrown)|Distance in Meters (Launched)|
|--|--|--|
|Close|2D6 - (net hits + 4)|2D6 - (net hits - 2) (note2)|
|Near|2D6 - (net hits + 2)|2D6 - net hits|
|Medium|2D6 - net hits|2D6 - (net hits -2)|
|Far (note1)|2D6 - (net hits - 2)|2D6 - (net hits - 4)|
|Extreme (note1)|2D6 - (net hits - 4)|2D6 - (net hits - 6)|

* Note 1: Most characters will not be able to reach these ranges with a thrown weapon.
* Note 2: Launchers will not fire here due to safety settings.

#### Scatter Diagram (SR6 pp 114)

![SR6 Scatter Diagram](images/scatter_diagram.png "SR6 Scatter Diagram")

### Weapon Ranges (SR6 pp 108)

* Close: 0-3 metres
* Near: 4-50 metres
* Medium: 51-250 metres
* Far: 251-500 metres
* Extreme: 500+ metres

### Firing Mode (SR6 pp 108,109)

Every weapon can fire in Single Shot mode even if not explicitly listed.

* SS (Single Shot)
  * No weapon attribute changes.
* SA (Semi-Automatic)
  * Fire two rounds with two trigger pulls.
  * -2 Attack Rating +1 Damage
* BF (Burst Fire)
  * Fire four rounds with a single trigger pull.
  * Narrow:
    * -4 Attack Rating
	* +2 Damage
  * Wide:
    * split dice between 2 targets
	* count each as an SA attack
* FA (Fully Automatic) 
  * Fire 10 rounds with a single trigger pull.
  * -6 Attack Rating.
  * Every target in 1 metre roll separate defence.
  * Can expand area by reducing Attack Rating by 2 for each 1 metre of radius.

## Exploding Dice

In these examples "x" should be replaced with an integer like 3, 6, 11, etc.

### Simple/Opposed Tests (SR6 pp 35)

Basic test dice: [x]

### Extended Tests (SR6 pp 36)

Extended test dice (the second x can only go to 9): [x]dx

### Iniative (SR6 pp 39)

Do initiative with Exploding Dice:

[6iX+Yd6,DR,CURRENTEDGE,EDGE,REACTION,INTUITION]

* X is your Initiative Score (Reaction+Intuition).
* Y is your number of Initiative Dice.
* CURRENTEDGE is your amount of edge at the start of the encounter.
* EDGE is your maximum edge score as shown on your character sheet.
* REACTION is your reaction stat.
* INTUITION is your intuition stat.

## Magic (SR6 pp 126)

### Assensing (SR6 pp 159)

(Assensing functions like a detection spell. Also see Detection Spells, SR6 pp 134, and Object Resistance, SR6 pp 129.)

* Astral+Intuition vs Body+Willpower (SR6 pp 134)
* Astral+Intuition vs Object Resistance (SR6 pp 129)

#### Assensing Table

* 0
  * none
* 1
  * general health
  * emotional state
  * mundane/awakened
* 2
  * location of standard cyberware
  * class+type of magic in use
  * aura (can remember with Memory test, SR6 pp 67)
* 3
  * location of alphaware
  * ballpark difference to assensor's Essence and Magic
  * ballpark difference between Force and assensor's Magic
  * general diagnosis of maladies
  * astral signatures
  * is a weapon focus an athame (SW pp 158)
* 4
  * location of betaware and bioware
  * literal Essence/Magic/Force
  * general cause of astral signature
  * is an athame charged (SW pp 158)
* 5
  * location of deltaware, gene treatments, nanotech
  * accurate diagnosis of maladies
  * if Technomancer/Monad

### Astral Combat (SR6 pp 160,161)

These instructions exclude spellcasting, see the Direct Combat Spells section for those details.

1. say your Attack Rating (Magic+Tradition Attribute)
1. compare to Defence Rating (Intuition+innate armour)
1. check other environmental advantages
1. distribute Edge
1. roll dice pool (see below) vs Willpower+Intuition
1. Damage is (see below) soaked by Willpower (pool of dice)

Dice pools:

* Unarmed Combat: Astral+Willpower
* Weapon Focus: Close Combat+Willpower

Damage values:

* Unarmed Combat: Tradition Attribute/2 (round up, choose S or P) + net hits
* Weapon Focus (SR6 pp 155,156): As weapon (choose S or P) + net hits

### Astral Tracking (SR6 pp 161)

* Astral + Intuition (5, 1 hour) Extended Test
* Each hour passed since the link was active: +1
* Target behind mana barrier: +(Force of barrier)
* Tracking master by spirit: +2

### Conjuring

#### Summoning A Spirit (SR6 pp 146,147)

1. determine level and type of desired spirit
1. roll Conjuring+Magic vs Spirit Level*2
   * net hits are services
1. resist Drain, Willpower+Tradition Attribute vs Spirit hits (threshold, not net hits)

* The drain damage is stun, or physical if after the resistance test the remaining damage is higher than Magic.
* If the summoner falls unconscious due to drain the spirit returns to its home plane.
* The summoner can spend reagents equal to the force of the sprit to get a bonus point of Edge.
* The summoner can have active spirits of combined force no greater than Magic*3.

#### Spirit Services (SR6 pp 147)

A single discrete, clearly defined action is a single service. If you need to take more than a single clause to say it, it's probably not clearly defined.

#### Banishing A Spirit (SR6 pp 147)

1. roll Conjuring+Magic vs Spirit Level*2
   * net hits reduce services by 1 per net hit
1. resist Drain, Willpower+Tradition Attribute vs Spirit hits*2 (threshold, not net hits)

* Banishing is a Major Action.
* The drain damage is stun, or physical if after the resistance test the remaining damage is higher than Magic.
* The banisher can spend reagents equal to the force of the sprit to get a bonus point of Edge.

### Counterspelling (SR6 pp 143)

#### Boosted Defence

* Counterspell Major Action
* roll Sorcery+Magic
* 2m sphere in LOS, +1m per dram of reagents
* net hits add to the defence roll against magic

#### Dispelling

* against sustained/ongoing spells
* Sorcery+Magic vs Drain(incl. adjustments)*2
* 1:1 net hits cancel spell net hits
* 0 spell net hits removes the spell

### Drain Resistance

* Willpower+Logic (Hermetic)
* Willpower+Charisma (Shamanic)

### Object Resistance (SR6 pp 129)

* Natural 3
* Low-tech 6
* High-tech 9
* Highly-processed 15

### Spellcasting

#### Direct Combat Spells (SR6 pp 132)

1. Say your Attack Rating (Magic+Tradition Attribute)
1. compare to Defence Rating (normal or astral)
   - normal (Body+Armour+Armour Effects)
   - astral (Intuition+Innate Armour+Armour Effects)
1. check other environmental advantages
1. distribute Edge
1. optional: pick spell adjustments
1. roll Sorcery+Magic vs Willpower+Intuition
1. Damage is Amp Up Damage+Net Hits, not soaked

#### Indirect Combat Spells (SR6 pp 132)

1. Say your Attack Rating (Magic+Tradition Attribute)
1. compare to Defence Rating (Body+Armour+Armour Effects)
1. check other environmental advantages
1. distribute Edge
1. optional: pick spell adjustments
1. roll Sorcery+Magic vs Willpower+Reaction
1. damage is Amp Up Damage+Net Hits+(Magic/2, round up), soaked by Body (pool of dice)
1. damage is the type listed in the spell description
1. if the target took at least 1 damage apply secondary effects per spell description

## Matrix (SR6 pp 170)

### Dumpshock (SR6 pp 176)

* Cold-Sim: 3S soaked with Willpower
* Hot-Sim: 3P soaked with Willpower
* Can't gain or spend Edge for (10 - Willpower) minutes.

### Glossary

* AR: Augmented Reality
* VR: Virtual Reality

### Living Persona Equivalences (SR6 pp 189)

Apart from what is below there are bonus points equal to Resonance to apply to any attribute.

A single attribute cannot be raised by more than 50% of its base rating, rounded up, maximum bonus applied +4.

|Matrix Attribute|Mental Attribute|
|---|---|
|Device Rating|Resonance|
|Attack|Charisma|
|Sleaze|Intuition|
|Data Processing|Logic|
|Firewall|Willpower|

### Matrix Combat

1. say your Attack Rating (persona Attack+Sleaze)
1. compare to Defence Rating (Data Processing+Firewall Effects)
1. check other environmental advantages
1. distribute Edge
1. roll Cracking+Logic vs Data Processing+Firewall
   * Possibly a different roll depending on the ability used!
1. Damage is per the ability used.
   * Tarpit: 1+Net Hits Matrix Damage, reduce target Data Processing by the same amount
   * Data Spike: (Attack/2,round up)+Net Hits Matrix Damage (resisted with Firewall)

### Matrix Initiative Scores Plus Dice (SR6 pp 179)

This includes the base 1D6 initiative dice that everybody gets. The CRB does not include the base dice.

* AR: Reaction+Intuition+1D6
* VR (cold sim): Intuition+Data Processing+2D6
* VR (hot sim): Intuition+Data Processing+3D6

(If a Technomancer, use the bonus-adjusted Data Processing score.)

(If a Rigger, use the RCC Data Processing score.)

### Matrix Perception (SR6 pp 178, 182)

Electronics+Intuition extended

* Search public information and databases for a subject.

Electronics+Intuition vs Willpower+Sleaze

* Analyze a single icon.
  * tie to perceive icon
  * single net hit to give device rating and icon name (like hostname)
  * 2+ net hits to give increasingly detailed information
* Search for hidden icon.

### Matrix Search (SR6 pp 183)

Electronics+Intuition extended (10 minute interval)

Search the Matrix for a topic, results as Legwork table (SR6 pp 50).

### Overwatch Score (SR6 pp 176)

* Matrix action modified by hacking program, +1.
* Illegal user-level access, +1 per round (see note).
* Illegal admin-level access, +3 per round (see note).
* Illegal actions, +1 per opposing dice roll hit.

Note: Use of Backdoor Entry (SR6 pp 180) and Known Exploit Backdoor Entry (H&S pp 30) can eliminate the per-round access penalties.

Maximum OS of 40, at which point:

1. Device condition monitor filled (stun for Technomancers).
2. Location reported to (local, national) authorities.
3. Dump Shock as forcibly disconnected from the Matrix.

### Notable Matrix Actions (SR6 pp 179,180)

These are useful to prepare for Matrix/Rigging work.

#### Jump Into Rigged Device (SR6 pp 182)

* legal
* Major Action
* needs User/Admin perms
* Electronics+Logic vs Willpower+Firewall
* Electronics+Logic vs Firewall*2
* no test needed if the device owner

#### Switch Interface Mode (SR6 pp 184)

* legal
* Minor Action
* needs Admin perms

Types of switch available:

* AR to VR (cold sim)
* AR to VR (hot sim)
* VR (any) to AR
* switch to Running Silent (SR6 pp 178)
* cancel Running Silent (SR6 pp 178)

#### Reconfigure Matrix Attribute (SR6 pp 183)

* legal
* Minor Action
* needs Admin perms
* perform one (1) x<->y swap
* Deckers swap whole attributes.
* Technomancers swap bonus points.

### Overwatch Score Increases (SR6 pp 176)

* +1 for each action modified by a hacking program
* +1 each round of user access per host
* +3 each round of admin access per host
* +1 per hit on opposed tests for illegal actions
  * not net hit, total hits

### Running Silent

See Matrix Perception for when somebody tries to find hidden icons.

### Sprites (SR6 pp 191)

#### Compiling A Sprite (SR6 pp 191)

1. determine level and type of desired sprite
1. roll Tasking+Resonance vs Sprite Level*2
   * net hits are tasks
1. resist Fading (Fade), Willpower+Charisma vs Sprite hits (threshold, not net hits)

The fading damage is stun, or physical if after the resistance test the remaining damage is higher than Resonance.

#### Registering A Sprite

1. duration is Sprite Level*hours
1. roll Tasking+Resonance vs Sprite Level*2
   * net hits are additional tasks, need min. 1 hit to successfully register the sprite
1. resist Fading, Willpower+Logic vs Sprite Hits*2 (threshold, not net hits)

#### Decompiling A Sprite

* roll Tasking+Resonance vs Sprite Level*2 (unregistered sprite)
* roll Tasking+Resonance vs Sprite Level+Resonance of compiling technomancer (registered sprite)

#### Sprite Tasks

Each of these entries consumes a single task.

##### Unregistered Sprite Tasks

* single use of single sprite power
* whole combat round of matrix actions
* fight in cybercombat until opponents are defeated or fled, sprite is decompiled, technomancer releases the sprite

If sent a a remote host the sprite will return to the Resonance after completing that task.

##### Registered Sprite Tasks

* all unregistered sprite tasks are included here
* tell sprite to follow somebody else's orders
* sprite will return after a remote order
* can re-register a sprite, if this fails and takes the last task the sprite returns to the Resonance
* tell the sprite to stand by, return to the Resonance to return when called
* have the sprite sustain a complex form for a number of rounds equal to its level

## Rigging (SR6 pp 196)

It will help to refer to the Matrix section of the rules for information on AR vs VR. Some Matrix Actions are listed there.

### Control Rig Bonuses (SR6 pp 283)

On tests this provides:

* +Rating dice pool
* -Rating threshold
* +1 bonus edge

### Control Rig Jumped In Attributes (SR6 pp 197)

|Regular Attribute|Jumped-In Attribute|
|---|---|
|Body|Willpower|
|Strength|Charisma|
|Agility|Logic|
|Reaction|Intuition|

### Rigger Command Console notes (SR6 pp 197)

* number of slaved drones equal to Rating*3
* issue a single command to any/all drones as Minor Action
* actions completed during drone initiative turn
* RCC Data Processing is used in VR initiative
* reduces Noise penalties by Rating
* protects slaved drones on the Matrix

### Vehicle Rigging (SR6 pp 198,199)

#### Summary

* Handling is threshold for tests while driving
* Acceleration is maximum speed increase per round
* for every Speed Interval crossed it is -1 dice pool for Handling tests, cumulative
* Top Speed is the maximum speed
* Body is how much damage the vehicle can take
* Armour is the extra protection of the vehicle's skin
* Pilot is the autopilot's piloting skill
* Sensor is the autopilot's Matrix Device Rating and ability to sense the surroundings

#### Tests

Pilot rolls:

* AR: Piloting+Reaction vs Handling (threshold)
* VR (Jumped In): Piloting+Intuition vs Handling (threshold)

Or use the stats for an opposed test with another pilot.

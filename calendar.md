Mayan Calendar Note
===================

In the Shadowrun fictional background the dragon Ryumyo smashed his way out of Mount Fuji in Japan on 24th December 2011. This was allegedly the start of the 13th baktun in the Mayan Calendar.

However it turns out that this might be the 14th baktun.

https://en.wikipedia.org/wiki/Baktun

It also turns out that when the conversion is done using a Mayan calendar converter the 13th baktun turned on December 21st 2012.

https://maya.nmai.si.edu/calendar/maya-calendar-converter

This was a scenario where a miscalculation was accepted going forward for canon fictional purposes.

https://www.reddit.com/r/Shadowrun/comments/4mgaha/is_the_sr5_core_rulebook_wrong_about_when_the/d3vfmec/

My explanation: the magic had waxed enough that the dragon Ryumyo could fly around, but not yet strictly calendar-13-baktun by the numbers.
